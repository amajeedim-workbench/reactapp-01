job('ReactApp-01 Build Pipeline') {
    scm {
        git{
            remote {
                url('https://bitbucket.org/amajeedim-workbench/reactapp-01.git')
            }
            browser {
                bitbucketWeb {
                    repoUrl('https://bitbucket.org/amajeedim-workbench/reactapp-01')
                }
            }
        }
    }

    triggers {
        bitBucketTrigger {
            overrideUrl('https://bitbucket.org/amajeedim-workbench/reactapp-01')
        }
    }

    wrappers {
        nodejs('JenkinsNodeJS')
    }

    steps {
        dockerBuildAndPublish {
            repositoryName('amajeedm/reactapp-01')
            registryCredentials('DockerID')
        }
    }
}